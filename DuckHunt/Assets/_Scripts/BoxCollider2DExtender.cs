﻿using UnityEngine;
using System.Collections;

/// <summary>
/// BoxCollider2D Extensions
/// </summary>
public static class BoxCollider2DExtender
{
    /// <summary>
    /// Get a random Vector3 within the BoxCollider2D.
    /// </summary>
    /// <param name="area"></param>
    /// <returns></returns>
    public static Vector3 GetRandomVector3(this BoxCollider2D area)
    {
        var bounds = area.bounds;
        var center = bounds.center;

        var x = UnityEngine.Random.Range(center.x - bounds.extents.x, center.x + bounds.extents.x);
        var y = UnityEngine.Random.Range(center.y - bounds.extents.y, center.y + bounds.extents.y);

        return new Vector3(x, y, 0);
    }
}
