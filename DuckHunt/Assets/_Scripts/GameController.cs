﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Singleton for managing curretn game state;
/// </summary>
public class GameController : MonoBehaviour
{
    #region public fields
    /// <summary>
    /// Spawning area for ducks.
    /// </summary>
    public BoxCollider2D _spawnArea;

    /// <summary>
    /// Prefab with the duck game object.
    /// </summary>
    public GameObject DuckPrefab;

    /// <summary>
    /// UI text element for duck counter.
    /// </summary>
    public Text DuckCountText;

    #endregion

    #region private fields

    /// <summary>
    /// Duck counter.
    /// </summary>
    private int _duckCount = 0;

    #endregion

    /// <summary>
    /// Process duck hit.
    /// </summary>
    public void DuckHit()
    {
        _duckCount++;
        DuckCountText.text = _duckCount.ToString();

        SpawnDuck();
    }

    /// <summary>
    /// Initialization.
    /// </summary>
    private void Start()
    {
        SpawnDuck();
    }
    
    /// <summary>
    /// Instantiate new duck.
    /// </summary>
    private void SpawnDuck()
    {
        Instantiate(DuckPrefab, _spawnArea.GetRandomVector3(), Quaternion.identity);
    }
}
