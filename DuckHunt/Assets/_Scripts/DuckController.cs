﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;


public class DuckController : MonoBehaviour, IPointerDownHandler
{
    #region enums

    private enum Direction
    {
        Up,
        Side,
        Diagonal
    }

    private enum HorizontalOrientation
    {
        Right,
        Left
    }

    #endregion

    #region private fields

    ///
    private bool _currentDestinationReached = true;

    private Vector3 _currentDestination = Vector3.zero;

    private BoxCollider2D _flyArea;

    private GameController _gameController;

    private bool _isHit = false;

    #endregion

    #region components

    private Transform _transform;
    private Animator _animator;
    private Rigidbody2D _rigidbody;

    #endregion

    #region public fields

    public float Speed = 2.5f;
    [Range(0f, 1f)] public float DirectionChangeProbability;

    #endregion

    /// <summary>
    /// Process Duck click.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        DuckHit();
    }

    /// <summary>
    /// Initialization.
    /// </summary>
    private void Start()
    {
        
    }

    /// <summary>
    /// Frame update for a duck.
    /// </summary>
    private void Update()
    {
    
    }

}
